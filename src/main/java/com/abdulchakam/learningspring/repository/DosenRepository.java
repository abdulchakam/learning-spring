package com.abdulchakam.learningspring.repository;

import com.abdulchakam.learningspring.model.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DosenRepository extends JpaRepository<Dosen, Long> {
}
