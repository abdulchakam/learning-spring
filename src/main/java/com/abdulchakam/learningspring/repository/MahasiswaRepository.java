package com.abdulchakam.learningspring.repository;

import com.abdulchakam.learningspring.model.Mahasiswa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Long> {
}
