package com.abdulchakam.learningspring.repository;

import com.abdulchakam.learningspring.model.Matkul;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatkulRepository extends JpaRepository<Matkul, Long> {
}
