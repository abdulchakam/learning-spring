package com.abdulchakam.learningspring.service;

import com.abdulchakam.learningspring.model.CustomMappingModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class CustomQueryDAO {

    @Autowired
    private EntityManager entityManager;

    public List<CustomMappingModel> getCustomQuery(String nipDosen){
        String nativeQuery = "SELECT dsn.nip, dsn.nama_dsn, mt.nama_matkul, mhs.nama_mhs FROM tbl_dosen dsn\n" +
                "INNER JOIN tbl_matkul mt ON mt.id = dsn.matkul_id\n" +
                "INNER JOIN tbl_mahasiswa mhs ON mhs.matkul_id = mt.id\n" +
                "WHERE dsn.nip = :nipDosen";
        Query query =entityManager.createNativeQuery(nativeQuery, "QueryNative1");
        List<CustomMappingModel> cusMapList = query.setParameter("nipDosen", nipDosen).getResultList();

        return  cusMapList;
    }

}
