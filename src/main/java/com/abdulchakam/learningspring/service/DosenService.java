package com.abdulchakam.learningspring.service;

import com.abdulchakam.learningspring.dto.DosenDto;
import com.abdulchakam.learningspring.model.Dosen;
import com.abdulchakam.learningspring.repository.DosenRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DosenService {

    @Autowired
    DosenRepository dosenRepository;

    public List<DosenDto> getDosenDto(){
        List<Dosen> dosens = dosenRepository.findAll();
        ModelMapper mapper = new ModelMapper();
        return dosens.stream().map(dosen -> mapper.map(dosen, DosenDto.class)).collect(Collectors.toList());
    }

}
