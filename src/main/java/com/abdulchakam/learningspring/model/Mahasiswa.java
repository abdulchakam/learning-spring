package com.abdulchakam.learningspring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_mahasiswa")
public class Mahasiswa {

    @Id
    private Long id;
    private String nim;
    private String nama_mhs;
    private String jns_kel;

    @ManyToOne()
    @JoinColumn(name = "matkul_id")
    @JsonBackReference
    private Matkul matkul;

}
