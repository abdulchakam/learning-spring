package com.abdulchakam.learningspring.model;

import lombok.Data;
import org.springframework.stereotype.Service;

import javax.persistence.*;

@Data
@Entity
@SqlResultSetMapping(name = "QueryNative1", entities = {
        @EntityResult(entityClass = CustomMappingModel.class, fields = {
                @FieldResult(name = "nipDosen", column = "nip"),
                @FieldResult(name = "namaDosen", column = "nama_dsn"),
                @FieldResult(name = "namaMatkul", column = "nama_matkul"),
                @FieldResult(name = "namaMhs", column = "nama_mhs"),
        })
})
public class CustomMappingModel {

    @Id
    private String nipDosen;
    private String namaDosen;
    private String namaMatkul;
    private String namaMhs;
}
