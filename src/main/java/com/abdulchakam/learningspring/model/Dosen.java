package com.abdulchakam.learningspring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "tbl_dosen")

public class Dosen {

    @Id
    private Long id;
    private String nip;
    private String nama_dsn;
    private String telp;
    private String email;

    @OneToOne()
    @JoinColumn(name = "matkul_id")
    private Matkul matkul;

}
