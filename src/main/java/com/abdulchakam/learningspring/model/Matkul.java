package com.abdulchakam.learningspring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_matkul")
@NoArgsConstructor
public class Matkul {
    @Id
    private Long id;
    private String nama_matkul;
    private Long sks;
    private Long semester;

    @OneToMany(mappedBy = "matkul")
    private List<Mahasiswa> mahasiswas;
    public Matkul(Long id){
        this.id = id;
    }

}
