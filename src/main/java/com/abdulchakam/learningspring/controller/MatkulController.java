package com.abdulchakam.learningspring.controller;

import com.abdulchakam.learningspring.model.Mahasiswa;
import com.abdulchakam.learningspring.model.Matkul;
import com.abdulchakam.learningspring.repository.MahasiswaRepository;
import com.abdulchakam.learningspring.repository.MatkulRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/api")
public class MatkulController {

    @Autowired
    private MatkulRepository matkulRepository;

    @GetMapping("/getAllMatkul")
    public ResponseEntity<List<Matkul>> getAllMatkul(){
        List<Matkul> matkuls = matkulRepository.findAll();
        if (matkuls.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(matkuls, HttpStatus.OK);
    }

    @PostMapping("/saveMatkulData")
    public ResponseEntity<Matkul> saveMatkul(@RequestBody Matkul matkul){
        try{
            Matkul matkulResult = matkulRepository.save(matkul);
            return new ResponseEntity<>(matkulResult, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error Save Data{}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @DeleteMapping("/deleteMatkulData")
    public ResponseEntity<String> deleteMatkul(@RequestParam Long idMatkul){
        try{
            matkulRepository.deleteById(idMatkul);
            return new ResponseEntity<>("Data Berhasil di Hapus", HttpStatus.OK);
        }catch (Exception e){
            log.error("Error Hapus Data{}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
