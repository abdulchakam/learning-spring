package com.abdulchakam.learningspring.controller;

import com.abdulchakam.learningspring.dto.DosenDto;
import com.abdulchakam.learningspring.model.CustomMappingModel;
import com.abdulchakam.learningspring.model.Dosen;
import com.abdulchakam.learningspring.repository.DosenRepository;
import com.abdulchakam.learningspring.service.CustomQueryDAO;
import com.abdulchakam.learningspring.service.DosenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/api")
public class DosenController {

    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private DosenService dosenService;

    @Autowired
    private CustomQueryDAO customQueryDAO;

    @GetMapping("/getAllDosen")
    public ResponseEntity getAllDosen(){
        List<Dosen> dosens = dosenRepository.findAll();
        return ResponseEntity.ok(dosens);

    }

    @PostMapping("/saveDosenData")
    public ResponseEntity<Dosen> saveDosen(@RequestBody Dosen dosen){
        try{
            Dosen dsn = dosenRepository.save(dosen);
            return new ResponseEntity<>(dsn, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error Save Data{}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @DeleteMapping("/deleteDosenData")
    public ResponseEntity<String> deleteDosen(@RequestParam Long idDosen){
        try{
            dosenRepository.deleteById(idDosen);
            return new ResponseEntity<>("Data Berhasil di Hapus", HttpStatus.OK);
        }catch (Exception e){
            log.error("Error Hapus Data{}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/getDosenDto")
    public ResponseEntity getDosenDto(){
        List<DosenDto> dosenDtos = dosenService.getDosenDto();
        return ResponseEntity.ok(dosenDtos);
    }

    @GetMapping("/nativeQuery")
    public ResponseEntity<List<CustomMappingModel>> getNativeQuery(@RequestParam String nipDosen){
        List<CustomMappingModel> list = customQueryDAO.getCustomQuery(nipDosen);
        return ResponseEntity.ok(list);
    }
}
