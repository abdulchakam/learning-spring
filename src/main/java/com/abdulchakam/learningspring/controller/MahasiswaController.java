package com.abdulchakam.learningspring.controller;

import com.abdulchakam.learningspring.model.Dosen;
import com.abdulchakam.learningspring.model.Mahasiswa;
import com.abdulchakam.learningspring.repository.DosenRepository;
import com.abdulchakam.learningspring.repository.MahasiswaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/api")
public class MahasiswaController {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @GetMapping("/getAllMahasiswa")
    public ResponseEntity<List<Mahasiswa>> getAllMahasiswa(){
        List<Mahasiswa> mahasiswas = mahasiswaRepository.findAll();
        if (mahasiswas.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(mahasiswas, HttpStatus.OK);
    }

    @PostMapping("/saveMahasiswaData")
    public ResponseEntity<Mahasiswa> saveMahasiswa(@RequestBody Mahasiswa mhs){
        try{
            Mahasiswa mahasiswas = mahasiswaRepository.save(mhs);
            return new ResponseEntity<>(mhs, HttpStatus.CREATED);
        }catch (Exception e){
            log.error("Error Save Data{}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @DeleteMapping("/deleteMasiswaData")
    public ResponseEntity<String> deleteMasiswa(@RequestParam Long idMahasiswa){
        try{
            mahasiswaRepository.deleteById(idMahasiswa);
            return new ResponseEntity<>("Data Berhasil di Hapus", HttpStatus.OK);
        }catch (Exception e){
            log.error("Error Hapus Data{}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
