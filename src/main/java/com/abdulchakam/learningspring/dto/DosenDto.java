package com.abdulchakam.learningspring.dto;

import lombok.Data;

@Data
public class DosenDto {
    private Long id;
    private String nip;
    private String nama;
}
